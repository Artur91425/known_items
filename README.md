## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/known_items/-/archive/master/known_items-master.zip)**
2. Unpack the Zip file
3. Rename the folder "known_items-master" to "known_items"
4. Copy "known_items" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Screenshots
<img src="https://user-images.githubusercontent.com/24303693/63620142-30eedd80-c5f9-11e9-8676-cb13a9951b95.jpg">
